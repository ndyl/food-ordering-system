package dyl.natalia.orders.model;

import java.math.BigDecimal;

public class LunchPart extends OrderItemImpl {
	private Cuisine cuisine;
	
	public LunchPart(String name, BigDecimal price, Cuisine cuisine) {
		super(name, price);
		this.cuisine = cuisine;
	}
	
	public Cuisine getCuisine() {
		return this.cuisine;
	}

	/*@Override
	public String toString() {
		return "LunchPart: " + this.getName();
	}*/
}
