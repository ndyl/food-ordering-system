package dyl.natalia.orders.model;

import java.math.BigDecimal;

public class Lunch implements OrderItem {
	private LunchPart mainCourse;
	private LunchPart dessert;
	
	public Lunch(LunchPart mainCourse, LunchPart dessert) {
		this.mainCourse = mainCourse;
		this.dessert = dessert;
	}

	@Override
	public BigDecimal getPrice() {
		BigDecimal mainCoursePrice = mainCourse.getPrice();
		BigDecimal dessertPrice = dessert.getPrice();
		BigDecimal lunchPrice = mainCoursePrice;
		lunchPrice.add(dessertPrice);
		return lunchPrice;
	}

	@Override
	public String getName() {
		String mainCourseName = mainCourse.getName();
		String dessertName = dessert.getName();
		return mainCourseName + " + " + dessertName;
	}

	@Override
	public String toString() {
		return "Lunch: " + this.getName();
	}
}
