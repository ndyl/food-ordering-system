package dyl.natalia.orders.model;

import static org.junit.Assert.*;
import static org.hamcrest.core.Is.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class OrderItemImplTest {
	private OrderItemImpl orderItem;
	
	@Before
	public void setUp() {
		orderItem = new OrderItemImpl("item", new BigDecimal(2.5));
	}

	//[the name of the tested method]_[expected input / tested state]_[expected behavior]
	//registerNewUserAccount_ExistingEmailAddressGiven_ShouldThrowException()
	@Test(expected = IllegalArgumentException.class)
	public void createOrderItemImpl_NegativePriceGiven_ShouldThrowException() {
		BigDecimal newPrice = new BigDecimal(-2.1);
		orderItem = new OrderItemImpl("item", newPrice);
	}

	@Test
	public void changePrice_PositivePriceGiven_ShouldChangePrice() {
		BigDecimal newPrice = new BigDecimal(2.1);
		orderItem.changePrice(newPrice);
		assertTrue(orderItem.getPrice().equals(newPrice));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void changePrice_NegativePriceGiven_ShouldThrowException() {
		BigDecimal newPrice = new BigDecimal(-2.1);
		orderItem.changePrice(newPrice);
	}
	
	@Test
	public void toString_NamePastaAndPrice2Given() {
		orderItem = new OrderItemImpl("Pasta", new BigDecimal(2.1));
		String expected = "OrderItemImpl: Pasta, price: 2.1";
		assertThat(orderItem.toString(), is(expected));
	}
}
