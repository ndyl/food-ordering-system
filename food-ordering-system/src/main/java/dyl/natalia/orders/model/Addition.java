package dyl.natalia.orders.model;

import java.math.BigDecimal;

public class Addition extends OrderItemImpl {
	
	private KindOfDrink kindOfDrink;
	
	public Addition(String name, BigDecimal price, KindOfDrink kindOfDrink) {
		super(name, price);
		this.kindOfDrink = kindOfDrink;
	}

	public KindOfDrink getKindOfDrink() {
		return this.kindOfDrink;
	}
	
	/*@Override
	public String toString() {
		return "Addition: " + this.getName();
	}*/
}
