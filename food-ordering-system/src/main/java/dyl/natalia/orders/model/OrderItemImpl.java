package dyl.natalia.orders.model;

import java.math.BigDecimal;

public class OrderItemImpl implements OrderItem {
	private final String name;
	private BigDecimal price;
	
	public OrderItemImpl(String name, BigDecimal price) {
		checkPrice(price);
		this.name = name;
		this.price = price;
	}
	
	public String getName() {
		return this.name;
	}

	public BigDecimal getPrice() {
		return this.price;
	}
	
	public void changePrice(BigDecimal newPrice) {
		checkPrice(newPrice);
		this.price = newPrice;
	}
	
	private void checkPrice(BigDecimal price) {
		if(price.compareTo(BigDecimal.valueOf(0.0)) < 0) {
			throw new IllegalArgumentException("Negative price value.");
		}
	}
	
	@Override
	public String toString() {
		String className = this.getClass().getSimpleName();
		double priceDouble = this.getPrice().doubleValue();
		return className + ": " + this.getName() + ", price: " + priceDouble;
	}
}
