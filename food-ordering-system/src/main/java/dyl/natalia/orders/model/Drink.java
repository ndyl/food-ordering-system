package dyl.natalia.orders.model;

import java.math.BigDecimal;
import java.util.*;

public class Drink implements OrderItem {
	private String name;
	private BigDecimal price;
	private KindOfDrink kindOfDrink;
	private List<Addition> additions;
	
	public Drink(String name, BigDecimal price, KindOfDrink kindOfDrink) {
		this.name = name;
		this.price = price;
		this.kindOfDrink = kindOfDrink;
		additions = new ArrayList<Addition>();
	}
	
	public KindOfDrink getKindOfDrink() {
		return this.kindOfDrink;
	}
	
	public void addAddition(Addition addition) {
		if(!additions.contains(addition)) {
			additions.add(addition);
		}
	}
	
	@Override
	public BigDecimal getPrice() {
		BigDecimal drinkAndAdditionsPrice = this.price;
		for(Addition addition : additions) {
			BigDecimal additionPrice = addition.getPrice();
			drinkAndAdditionsPrice.add(additionPrice);
		}
		
		return drinkAndAdditionsPrice;
	}

	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public String toString() {
		return "Drink: " + this.getName();
	}
}
