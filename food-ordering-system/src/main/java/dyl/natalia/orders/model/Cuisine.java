package dyl.natalia.orders.model;

public enum Cuisine {
	POLISH,
	MEXICAN,
	ITALIAN
}
