package dyl.natalia.orders.model;

import java.math.BigDecimal;

public interface OrderItem {
	public BigDecimal getPrice();
	public String getName();
}
